const jokes = [
    {
        id: 513,
        joke: "Chuck Norris does not code in cycles, he codes in strikes.",
        categories: ["nerdy"],
    },
    {
        id: 423,
        joke:
     "Chuck Norris had to stop washing his clothes in the ocean. The tsunamis were killing people.",
        categories: [],
    }];
    

const nerdyJokes = jokes.filter(entry => entry.categories.includes("nerdy"));
console.log(nerdyJokes);