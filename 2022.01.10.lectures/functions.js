"use strict";

const singleButton = document.getElementById("showsingle");
singleButton.addEventListener("click", () => { displayRandomJoke(); });

const nerdyButton = document.getElementById("shownerdy");
nerdyButton.addEventListener("click", () => { displayNerdyJoke(); });

const allButton = document.getElementById("showall");
allButton.addEventListener("click", () => { displayAllJokes(); });

function getRandomInt(max) {
    return Math.floor(Math.random()*max);
}

function displayRandomJoke() {

    const jokeCount = jokes.length;
    const jokeID = getRandomInt(jokeCount);
    const joke = jokes[jokeID].joke;
    window.alert(joke);

}

function displayNerdyJoke() {

    const nerdyJokes = jokes.filter(entry => entry.categories.includes("nerdy"));
    const jokeCount = nerdyJokes.length;
    const jokeID = getRandomInt(jokeCount);
    const joke = nerdyJokes[jokeID].joke;
    window.alert(joke);

}

function displayAllJokes() {

    const jokeMessages = jokes.reduce( (prevValue,curValue) => prevValue + curValue.joke +"\n","");
    window.alert(jokeMessages);
}