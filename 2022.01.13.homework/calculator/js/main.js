"use strict";

let displayString = "";
const operations = ["+","-","*","/"];

let equalPressedStatus = false;

document.onload = initialize();
function initialize() {

    // add listener to numbers
    for (let i=0;i<=9;i++) {
        const buttonString = "n" + i;        
        document.getElementById(buttonString).addEventListener("click", () => { numberPressed(i); });
    }

    // add listener to clear
    document.getElementById("clear").addEventListener("click", () => {clearPressed(); });

    // add listener to decimal point 
    document.getElementById("decimal").addEventListener("click", () => {decimalPressed(); });

    // add listener to operators
    for (let i=0; i<operations.length;i++) {
        document.getElementById(operations[i]).addEventListener("click", () => { operatorPressed(operations[i]); });
    }

    // add listener to equal sign
    document.getElementById("equal").addEventListener("click", () => { equalPressed(); });
}

function numberPressed(number) {

    resetIfNecessary();

    displayString += number;
    refreshDisplay();
}

function refreshDisplay() {
    const displayElement = document.getElementById("display");
    displayElement.textContent = displayString;
}

function clearPressed() {
    displayString = "0";
    refreshDisplay();
}

function decimalPressed() {

    resetIfNecessary();

    // $ at the end of string => only test later number
    let reFindDot = /[0-9]+\.[0-9]*$/;
    let reFindOP = /[\+\-\*\/]$/;

    // if decimal point pressed after operator string, add a zero
    if (reFindOP.test(displayString)) {
        displayString += "0.";
    } 
    // only add decimal point if the later number doesn't already include a decimal point
    else if (!reFindDot.test(displayString)) {
        displayString += ".";
    }
    refreshDisplay();
}

function operatorPressed(operationString) {

    resetIfNecessary();

    // operator can't start the string
    if (displayString !== "0" && displayString !== "") {
        displayString += operationString; 
        refreshDisplay();
    }
}

function equalPressed() {

    // Uses Mathjs.org : math.js
    const result = math.evaluate(displayString);
    displayString = result.toString();
    refreshDisplay();
    equalPressedStatus = true;
}

// if user just pressed = button, then after displaying the result, reset the display
function resetIfNecessary() {

    if (equalPressedStatus || displayString === "0") { 
        displayString = "";
        equalPressedStatus = false;
    }
}