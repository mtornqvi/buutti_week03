import axios from "axios";

const urlOne = "https://jsonplaceholder.typicode.com/todos/";
const urlTwo = "https://jsonplaceholder.typicode.com/users/3";
const requests = [axios.get(urlOne),axios.get(urlTwo)];

try {
    const responses = await axios.all(requests);
    const tasks = responses[0].data.filter(task => (task.userId === 3, task.id === 41));
    const task = tasks[0];
    // console.log(task);

    const userData = responses[1].data;
    // console.log(userData);

    task.user = userData;
    console.log(task);


} catch (error) {
    console.error(error.message);
}