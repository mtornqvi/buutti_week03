import axios from "axios";

const url = "https://jsonplaceholder.typicode.com/todos/";


try {
    const response = await axios.get(url);
    console.log(response.data);
} catch (error) {
    console.error(error.message);
}