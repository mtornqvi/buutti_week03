"use strict";

let submissions = {};

const addButton = document.getElementById("addbutton");
addButton.addEventListener("click", () => { toggleAddUI(); });

function initialize() {

    toggleAddUI();
    displaySubmissions();
}

// note : this function doesn't use the normal POST method, as backend functions have not yet been 
// introduced in the course (just click evenets)
function toggleAddUI() {

    if (document.getElementById("submissionarea").innerHTML === "") {   
        let someHtml =  "<label for='name'><h2>Name:</h2></label>" + "<br/>" +
        "<input type='text' id='nameBox' name='name' />" + "<br/><br/>" +
        "<label for='post'><h2>Post:</h2></label>" + "<br/>" +
        "<textarea id='postBox' name='post' rows='10' cols='50'></textarea>" + "<br/>" +
        "<button id='submitbutton'>Submit!</button>" ;
        document.getElementById("submissionarea").innerHTML = someHtml;

        const submitButton = document.getElementById("submitbutton");

        submitButton.addEventListener("click", () => { recordData(); });

    } else if (document.getElementById("submissionarea").style.visibility === "hidden") {
        document.getElementById("submissionarea").style.visibility = "visible";
        document.getElementById("submissionarea").style.height = "auto";
    } else {
        document.getElementById("submissionarea").style.visibility = "hidden";
        document.getElementById("submissionarea").style.height = "0px";
    }
}



function displaySubmissions() {
    let someHtml = "";
    const submissionsCount = Object.keys(submissions).length;
    for (let i=0;i<submissionsCount; i++) {

        someHtml += 
            "<div class='postingbox'>" +
            "<h2><u>" +`${submissions[i].name}` + "</h2></u>" + "<span class='deletebutton'> " +
            "<button class='button' id=" + "delete" + `${i}` +"button > Delete </button> " + 
            "</span>" + "<br/>" + 
            "<p>" + `${submissions[i].post}` + "</p>" +
            "</div>";
    }
    
    document.getElementById("postsarea").innerHTML = someHtml;

    // add delete button event listeners
    for (let i=0;i<submissionsCount; i++) {
        const buttonName = "delete" + `${i}` + "button";
        const deleteButton = document.getElementById(buttonName);
        deleteButton.addEventListener("click", () => { deletePost(i); });
    }

}

function deletePost(postIndex) {
    delete submissions[postIndex]; // remove one posting
    displaySubmissions(); // redisplay the remaining
}

document.onload = initialize();

function recordData() {

    const submissionsCount = Object.keys(submissions).length;
    submissions[submissionsCount] = {}; // must initialize
    const nameInput = document.getElementById("nameBox").value;
    const postInput = document.getElementById("postBox").value;
    if (nameInput === "" || postInput === "") {
        window.alert("Please do not leave the fields empty");
        delete submissions[submissionsCount]; // remove the unfinished posting
    } else {
        submissions[submissionsCount].name = nameInput;
        submissions[submissionsCount].post = postInput;
        // clear the fields
        document.getElementById("nameBox").value = "";
        document.getElementById("postBox").value = "";
    }

    displaySubmissions();
}