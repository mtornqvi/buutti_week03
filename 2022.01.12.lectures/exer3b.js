const getValue = function () {
    return new Promise((res, rej) => {
        setTimeout(() => {
            res({ value: Math.random() });
        }, Math.random() * 1500);
    });
};

// using promise then
getValue()
    .then( (ans) => { console.log (`Value 1 is ${ans.value}`); });
getValue()
    .then( (ans) => { console.log (`Value 2 is ${ans.value}`); });