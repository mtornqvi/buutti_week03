const messages = ["3","..2","....1","GO!"];

// foreach requires callback : place second callback setTimout inside it
messages.forEach( (element,index) => {
    setTimeout( () => {
        console.log(element);
    }, 1000*index);
}
);
