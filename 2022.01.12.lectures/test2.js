new Promise( (resolve,reject) => {
    const it_works = true;
    if (it_works) {
        resolve("resolved!");
    } else {
        reject("rejected!");
    }
}).then( (success) => {
    console.log(`It returned ${success}`);
}).catch((err) => {
    console.log(`It returned ${err}`);
});