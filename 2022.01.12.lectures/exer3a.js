const getValue = function () {
    return new Promise((res, rej) => {
        setTimeout(() => {
            res({ value: Math.random() });
        }, Math.random() * 1500);
    });
};

// using async + await
const printAnswers = async() => {
    let ans = await getValue();
    console.log(`Value 1 is ${ans.value}`);
    ans = await getValue();
    console.log(`Value 2 is ${ans.value}`);

};

printAnswers();